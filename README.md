# Test-Driven Development with Python, Flask, and Docker

[![pipeline status](https://gitlab.com/odusseus.development/flask-tdd-docker/badges/master/pipeline.svg)](https://gitlab.com/odusseus.development/flask-tdd-docker/commits/master)


## Project description

This project sets up a development enviroment with Docker in order to build and deploy a microservice powered by Python and Flask. Practices of Test-Driven Development are applied during the development of RESTful API. The project was created based on the course [Test-Driven Development with Python, Flask, and Docker](https://testdriven.io/courses/tdd-flask/). 

### Tools, technologies and services used:

- Python
- Flask
- Docker
- Postgres
- Gunicorn
- Swagger/OpenAPI
- Flask-RESTX
- Flask-SQLAlchemy
- Flask-Admin
- pytest
- Coverage.py
- Flake8
- Black
- isort
- HTTPie
- GitLab
- Heroku
