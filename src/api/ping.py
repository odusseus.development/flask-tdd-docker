# src/api/ping.py

from flask_restx import Namespace, Resource  # type: ignore

ping_namespace = Namespace("ping")


class Ping(Resource):
    def get(self):
        return {"status": "success", "message": "Kaji <3"}


ping_namespace.add_resource(Ping, "")
